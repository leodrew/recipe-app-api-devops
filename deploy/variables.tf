variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "test@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}


variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "247459318694.dkr.ecr.us-east-1.amazonaws.com/django-api-proxy:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for porxy"
  default     = "247459318694.dkr.ecr.us-east-1.amazonaws.com/django-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}